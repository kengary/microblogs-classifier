This dataset contains tweets from three organitions: NUS, DBS, and STARHUB.

There are 3000 samples for each organization: 1500 positive ones and 1500 negative ones.

The data is in json format, which contains all available information provided by Twitter.
For details about the defination of each field, please refer to
https://dev.twitter.com/docs/platform-objects/tweets
If you need to get more information (e.g., the social links between users), you could use the Twitter API:
https://dev.twitter.com/docs/api/1.1

For each organization, you should use the first 750 (1-750) positive and negative tweets as training set, and use the rest 
750 (751-1500) positive and negative tweets as testing set. You should report your classification results on the testing sets.

A simple java program is provided to demonstrate how to read the json data, get specific data field, and generate word index.

A new file with several new tweets (in the same json format) will be used to test your program. 
Please notice that the new tweets may contain words that are never shown in the training and testing dataset.

This dataset contains original data crowled from Twitter. 
Due to privacy issues, please do not public this dataset to anyone or for any use outside the class.